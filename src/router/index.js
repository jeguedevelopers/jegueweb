import Vue from 'vue'
import Router from 'vue-router'
import VueResource from 'vue-resource'
import Trending from '@/components/Trending'
import Video from '@/components/Video'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Channel from '@/components/Channel'
import Upload from '@/components/Upload'
import Logout from '@/components/Logout'
import UploadVideo from '@/components/UploadVideo'
import UserChannel from '@/components/UserChannel'

Vue.use(Router)
Vue.use(VueResource)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Trending',
      component: Trending
    },
    {
      path: '/video/:id',
      name: 'Video',
      component: Video
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/channel',
      name: 'Channel',
      component: Channel
    },
    {
      path: '/upload',
      name: 'Upload',
      component: Upload
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout
    },
    {
      path: '/upload/:id',
      name: 'UploadVideo',
      component: UploadVideo
    },
    {
      path: '/users/:username',
      name: 'UserChannel',
      component: UserChannel
    }
  ]
})
