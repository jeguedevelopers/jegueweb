export function parseLoginError (error) {
  switch (error) {
    case 'INVALID_EMAIL_OR_PASSWORD':
      return 'Email ou senha inválidos'
    default:
      return 'Ops! Houve um erro. Tente novamente mais tarde'
  }
}

export function parseRegisterError (error) {
  switch (error) {
    case 'PASSWORD_CONFIRMATION_FAILURE':
      return 'Falha na confirmação da senha'
    case 'EMAIL_ALREADY_TAKEN':
      return 'Esse usuário já está cadastrado'
    default:
      return 'Ops! Houve um erro. Tente novamente mais tarde'
  }
}

export function parseVideoError (error) {
  switch (error) {
    case 'NAME_TOO_SMALL':
      return 'Adicione um nome maior ao seu vídeo'
    case 'UNAUTHORIZED_USER':
      return 'Este usuário não possui permições para essa ação'
    default:
      return 'Ops! Houve um erro. Tente novamente mais tarde'
  }
}

export function parseVideoUploadError (error) {
  switch (error) {
    case 'VIDEO_NOT_FOUND':
      return 'Vídeo não foi encontrado'
    case 'VIDEO_ALREADY_UPLOADED':
      return 'Este vídeo já existe no site'
    default:
      return 'Ops! Houve um erro. Tente novamente mais tarde'
  }
}

