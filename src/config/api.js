const baseUrl = 'https://api.jeguestreaming.tk'
export default {
  login: `${baseUrl}/login`,
  register: `${baseUrl}/register`,
  video: `${baseUrl}/videos`,
  home: `${baseUrl}/home`,
  channel: `${baseUrl}/users`
}
