import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    user: null
  },
  mutations: {
    login (state, user) {
      this.state.user = user
    },
    logout () {
      this.state.user = null
    }
  },
  plugins: [createPersistedState()]
})
