import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App'
import router from './router'
import { store } from './store'

Vue.config.productionTip = false

Vue.use(VueResource)

import 'font-awesome/css/font-awesome.css'
import '@/assets/css/flatly.min.css'
import '@/assets/css/style.css'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})
