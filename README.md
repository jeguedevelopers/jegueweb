# JegueStreaming Web Client

A Jegue Corporation orgulhosamente apresenta seu cliente web oficial.

## Instalação

* Instale o Node Package Manager:

`sudo apt install npm`

`sudo apt-get install nodejs-legacy`

* Instale o Vue

`sudo npm install -g vue-cli`

* Acesse o diretório e instale os pacotes Node

`cd jegueweb && npm install`

* Rode o servidor

`npm run dev`

O servidor local deve ser lançado, com o endereço `localhost:8080`.

* That's all, folks.